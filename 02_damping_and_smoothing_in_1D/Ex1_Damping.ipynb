{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<center>\n",
    "    <h1> Regularization in 1D: Damping</h1>\n",
    "    \n",
    "| Contact | Email   |\n",
    "|----|---|\n",
    "| Prof. Dr. Florian Wagner  | <fwagner@gim.rwth-aachen.de> |\n",
    "</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from numpy.linalg import inv\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Zero-offset profiling\n",
    "\n",
    "<img src=\"Profile.png\" style=\"width: 70%;\">\n",
    "\n",
    "A zero-offset seismic depth sounding is conducted with the aim of detecting the subsurface topography of a bedrock, overlain with sediments (see figure above). The surface is located at $z = 0$, and measurements are conducted in equidistant intervals (positions $x_b$).\n",
    "\n",
    "A simplified seismic model view is assumed: A seismic signal is generated at a source location and the signal is then assumed to travel downwards and reflected at the top of the bedrock. A receiver at the source location then picks up the reflected signal. Measurements are thus travel times (in seconds of the seismic wave from source to bedrock and back $\\rightarrow$ TWT, Two-way traveltimes). The P-wave velocity of the sediment layer is assumed to be $v = 2500$ m/s.\n",
    "\n",
    "A transect of $100$ m length is measured, starting a $x=0$, ending at $x=100$ m. Measurements are equally spaced (with $N_d$ the number of measurements - changes depending on the subtask). The bedrock topography in reality is continuous ($z$); however, for the sake of simplicity (and applicability), assume a finite number of bedrock segments, with a constant depth for each segment ($z_b$) as model parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 1**: Describe the model parameters, data parameters, and auxiliary variables and their units. Under which circumstances is the problem unique, over-, mixed-, or under-determined?\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 2**: Uniquely-determined problem\n",
    "\n",
    "Formulate the linear inverse problem for the *unique* (also known as even-determined) problem, given 10 equally-spaced measurements. Adjust the number of bedrock segments accordingly.\n",
    "\n",
    "**Estimate the bedrock topography** for the following data & measurement positions (copy this code into your solution:\n",
    "``` python\n",
    "import data\n",
    "d = data.get_data() # Importing the measured data\n",
    "x_b = np.linspace(0, 100, 10) # Creating mesurement positions\n",
    "d, x_b\n",
    "```\n",
    "\n",
    "In addition to the data and measurements we know the seismic velocity $v = 2500$, e.g. from borehole measurements. From these parameters, we try to estimate $z_b$. Perform the inversion by setting up a G matrix and using the appropriate mathematical solution (hint: diagonal matrices can be created conveniently by first creating an [identity matrix](https://numpy.org/doc/stable/reference/generated/numpy.eye.html))\n",
    "\n",
    "Plot the resulting (discrete) topography using the `plt.step` [function](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.step.html) (with the argument `where=\"mid\"`). Also have a look at the [axhline](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.axhline.html) and [vlines](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.vlines.html) functions to plot the earth surface and the raypaths. Compare it to the true topography, which you can obtain via `data.get_true_topography()`.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 3**: Now assume an under-determined problem, with only 4 measurements and 10 bedrock segments (i.e., model parameters). Estimate the bedrock depth for the sparse data set using the **damped-least squares solution** (often used for mixed-determined problems). Remember to keep the number of parameters (of the bedrock depth) the same as in (b). You can use the code snippet below to only use a subset of the data from task b (and also just a subset of rows in $\\mathbf{G}$):\n",
    "\n",
    "``` python\n",
    "x_b_sparse = x_b[::3] # every third measurement location\n",
    "d_sparse = d[::3] # every third measurement\n",
    "```\n",
    "\n",
    "Use damping constraints ($\\mathbf{W}_m = I$) to solve the least-squares problem. Vary the regularization parameter $\\beta^2$ between $10^{-10}$ and $1$ and describe the results. Which value would you choose? Plot the final bedrock estimation.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 4**: What happens if the damping factor is zero?\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Bonus question\n",
    "<div class=\"alert alert-success\">\n",
    "\n",
    "Compare the result of the damped least-squares solution (with a small damping factor) to the minimum-norm solution.\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "base",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
