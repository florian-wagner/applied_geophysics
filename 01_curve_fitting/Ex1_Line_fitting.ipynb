{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<center>\n",
    "    <h1> Curve Fitting: Fitting a straight line</h1>\n",
    "    \n",
    "| Contact | Email   |\n",
    "|----|---|\n",
    "| Prof. Dr. Florian Wagner  | <fwagner@gim.rwth-aachen.de> |\n",
    "</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Importing numpy for calculations and matplotlib for plotting\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following exercises, you will perform curve fitting exercises of increasing complexity. Curve Fitting represents an inverse problem as you try to find mathematical model parameters that best describe given data. While several implementations for regression problems are available in Python libraries such as Numpy's [polyfit](https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html) or [linear algebra solver](https://numpy.org/doc/stable/reference/generated/numpy.linalg.solve.html?highlight=solve#numpy.linalg.solve), please solve the exercises by calculating the appropriate generalized inverse as introduced in the lecture as it will help you to grasp the concept.\n",
    "\n",
    "For this, you will need to perform [matrix inversions](https://numpy.org/doc/stable/reference/generated/numpy.linalg.inv.html?highlight=inv#numpy.linalg.inv), [matrix transposes](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.T.html#numpy.ndarray.T), as well as matrix-matrix and matrix-vector [multiplications](https://numpy.org/doc/stable/reference/generated/numpy.matmul.html?highlight=matmul#numpy.matmul). Note that the matrix-multiplication function `C = np.matmul(A,B)` can also be performed by the `@`-operator, e.g. `C = A @ B`. Using the multiplication sign (`*`) on two matrices will **not** return their product, but will perform element-wise multiplication instead. You can also multiply an array with a scalar to multiply each individual element by that value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Linear Models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 Fitting a linear model with few observations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assume we have two measurements $d_1$ and $d_2$, that were taken from a linear process that can be described by $d(x) = a \\cdot x + b$ :\n",
    "\n",
    "$d_1 = 13$ and $d_2 = 7$, taken at $x_1 = 10$ and $x_2 = 15$\n",
    "\n",
    "<div class=\"alert alert-success\">\n",
    "\n",
    "**Task 1a**: Create vectors for data and x-values, and visualize the two datapoints in a [scatterplot](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.pyplot.scatter.html):\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 1b**: Which type of problem is posed here? What are the auxiliary variables of this problem? Which solution is appropriate to solve the inverse problem?\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 1c**: What are the dimensions of the **G** matrix? Write it down on paper first and then construct it into a numpy array.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 1d**: Using the appropriate solution from task 1b, estimate the model parameters $a$ and $b$ given the observations.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "Check your solution for a and b by using them to get a data estimate at $x = 10$ and $x = 15$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 1e**:  Visualize a range of model realizations from $x = -50$ to $x = 50$ and plot the original data against it. We can use Numpy's [linspace](https://numpy.org/doc/stable/reference/generated/numpy.linspace.html) function to create an auxiliary variable x.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## 1.2 Fitting a linear model with many observations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "We have given 25 data points ($d_1$) of a hypothetical measurement taken at 25 different positions ($x_1$) of a similar linear process as in the previous task:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "d1 = np.array([  7.93494028,  5.68739874,  2.6285696 , 12.21816911, 11.94169231,\n",
    "                 4.13625885,  8.51225942,  5.62103488,  5.65592905, 22.08629139,\n",
    "                12.44603838, 18.49236748, 17.7113576 , 17.360048  , 12.79941666,\n",
    "                13.92529062, 22.1420152 , 26.53605093, 12.65966116, 18.11449018,\n",
    "                22.75746405, 29.95185639, 28.37414316, 30.70226002, 28.8055315 ])\n",
    "\n",
    "x1 = np.linspace(0,10,25)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 2a**: What type of problem is presented here? Which solution is appropriate to solve it?\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 2b**: Explicitly write down **G** and create it as numpy array.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 2c**: Estimate the model parameters (and put this estimation in a function for later reuse).\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 2d**: Visualize the measured data together with the predicted data and again put this into a function for later reuse.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    \n",
    "**Task 2e**: Compute the RMS (\"root mean square\") error between the given observations and the model response, i.e. the predicted data.\n",
    "\n",
    "</div>\n",
    "\n",
    "$$RMSE = \\sqrt{\\frac{1}{N} \\sum_{i=1}^N ( d^i - d^i_\\mathrm{pre}))^2 }$$\n",
    "\n",
    "Again, put this into a function."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
