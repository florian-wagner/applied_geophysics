# Inversion concepts for multi-method geophysics (WiSe 24/25)

Florian Wagner (<florian.wagner@gim.rwth-aachen.de>)

---

You can download the whole folder as a zip-archive (Download button in the upper right) or single files individually.
Note that this is only necessary if you want to use your local Python installation. 

All others can simply click this button
[![](https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg)](https://jupyter.rwth-aachen.de/hub/spawn?profile=agp)
